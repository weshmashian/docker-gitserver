FROM alpine:latest

RUN apk update && apk add git git-daemon git-gitweb nginx spawn-fcgi fcgiwrap perl-cgi apache2-utils && \
    echo "/usr/libexec/git-core/git-shell" >> /etc/shells

ADD configs/src/ /

VOLUME /srv/git
EXPOSE 80 9418
CMD ["/bin/sh", "/start.sh" ]
