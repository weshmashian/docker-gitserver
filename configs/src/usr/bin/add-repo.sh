#!/usr/bin/env sh

BASEDIR="/srv/git"

if [ $# -eq 0 ] ; then
    echo "Usage: $0 name [name1 name2 ... nameN]" >&2
    echo ""
    echo "Create named repositories in ${BASEDIR}"
    exit 1
fi

while [ $# -gt 0 ] ; do
    REPO=$1
    shift

    BASE="${BASEDIR}/${REPO}.git"

    if [ -d "${BASE}" ] ; then
        echo "${REPO} already exists, skipping..." >&2
        continue
    fi

    su -l git -s /bin/sh -c "mkdir -p ${BASE}"
    su -l git -s /bin/sh -c "git init --bare ${BASE}"
    mv $BASE/hooks/post-update.sample $BASE/hooks/post-update
done
