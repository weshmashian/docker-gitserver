#!/usr/bin/env ash

_term() {
    for PIDFILE in /run/nginx/nginx.pid /var/run/git-daemon.pid ; do
        test -e $PIDFILE && kill -SIGTERM $(cat $PIDFILE)
    done
    killall -SIGTERM fcgiwrap
}

trap _term SIGTERM

GIT_UID=${GIT_UID:-1000}
GIT_GID=${GIT_GID:-1000}

HTTP_AUTH=${HTTP_AUTH:-on}
USERS=${USERS:-git:git}

GIT_DAEMON=${GIT_DAEMON:-on}
GIT_DAEMON_ARGS=${GIT_DAEMON_ARGS:---export-all}

FCGIOPTS="-n -u git -G nginx -M 0660 -s /var/run/fcgiwrap.sock"

getent group git  >/dev/null || addgroup -g $GIT_GID git
getent passwd git >/dev/null || adduser -D -u $GIT_UID -G git -s /usr/libexec/git-core/git-shell git

if [ ! -e /srv/git ] ; then
    mkdir -p /srv/git
fi

chown git.git /srv/git

if [ "${HTTP_AUTH}" = "on" ] ; then
    if [ ! -e /etc/nginx/htpasswd ] ; then
        touch /etc/nginx/htpasswd
        for USER in $USERS ; do
            htpasswd -b -B /etc/nginx/htpasswd $(echo $USER | tr ':' ' ')
        done
    fi
else
    sed -i -re '/# BEGIN AUTH/,/# END AUTH/d' /etc/nginx/conf.d/default.conf
fi

if [ "${GIT_DAEMON}" = "on" ] ; then
    /usr/libexec/git-core/git-daemon --base-path=/srv/git --reuseaddr --detach --pid-file=/var/run/git-daemon.pid $GIT_DAEMON_ARGS /srv/git
fi

HOME=/home/git /usr/bin/spawn-fcgi $FCGIOPTS -- /usr/bin/fcgiwrap &

mkdir -p /run/nginx
/usr/sbin/nginx

wait $!

