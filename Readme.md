

# Docker git-server

This is a small and simple container for running a git server using `nginx`, `gitweb`, and
`git-daemon`. Using SSH to access repositories is not supported, nor planned.


# Configuration


## Environment variables

-   **HTTP\_AUTH:** Use basic HTTP authentication for `gitweb` access. Default: on.
-   **USERS:** Space separated list of `user:pass` combinations to add to `htpasswd` file. This has no
    effect if the file already exists in the container, or if `HTTP_AUTH` is not enabled.
    Default: `git:git`.
-   **GIT\_DAEMON:** Should `git-daemon` be running inside the container. Change to any value to turn it
    off. Default: `on`.
-   **GIT\_DAEMON\_ARGS:** Additional options for `git-daemon`. Default: `--export-all`.
-   **GIT\_UID, GIT\_GID:** User and group ID of the `git` user inside the container. Useful if `/srv/git`
    numeric UID/GID need to match those outside of the container. Default: `1000`.


# Example

Running the container:

    docker volume create gitserver-data
    docker run -d --name gitserver -p 80:80 -p 9148:9148 -v gitserver-data:/srv/git gitserver:latest

Adding repositories:

    docker exec gitserver add-repo.sh repo1 repo2 # ... repoN

