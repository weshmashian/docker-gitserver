#+STARTUP: indent hidestars
#+OPTIONS: date:nil prop:nil tags:nil todo:nil toc:nil timestamp:nil author:nil num:nil
#+TITLE: Docker git-server

* {{{title}}}

This is a small and simple container for running a git server using =nginx=, =gitweb=, and
=git-daemon=. Using SSH to access repositories is not supported, nor planned.

* Configuration

** Environment variables

- HTTP_AUTH :: Use basic HTTP authentication for =gitweb= access. Default: on.
- USERS :: Space separated list of =user:pass= combinations to add to =htpasswd= file. This has no
           effect if the file already exists in the container, or if =HTTP_AUTH= is not enabled.
           Default: =git:git=.
- GIT_DAEMON :: Should =git-daemon= be running inside the container. Change to any value to turn it
                off. Default: =on=.
- GIT_DAEMON_ARGS :: Additional options for =git-daemon=. Default: =--export-all=.
- GIT_UID, GIT_GID :: User and group ID of the =git= user inside the container. Useful if =/srv/git=
     numeric UID/GID need to match those outside of the container. Default: =1000=.

* Example
Running the container:
#+BEGIN_SRC sh :exports code
docker volume create gitserver-data
docker run -d --name gitserver -p 80:80 -p 9148:9148 -v gitserver-data:/srv/git gitserver:latest
#+END_SRC

Adding repositories:
#+BEGIN_SRC sh :exports code
docker exec gitserver add-repo.sh repo1 repo2 # ... repoN
#+END_SRC
* Tasks                                                            :noexport:
